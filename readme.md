# Arduino ENC28J60
Find ETH_Serial directory - it is chrome app.
Use index.html file for working with your Arduino board throught TCP/IP http server-client without serial port.
If you need serial port(USB) - you must run this index.html as chrome app (without this your browser have not permissions for seprial port). In pictures bellow i tried to show how to do it.... 

For Arduino board firmware WebServer.ino. Need library for working with ENC28J60 = UIPEthernet. 
Place this UIPEthernet folder in:
DIR_OF_YOUR ARDUINO_IDE/libraries/



Arduino IDE 1.6 with all need libs:
https://drive.google.com/file/d/0ByYhW8gwlrVuNTVnc1FQSkozMjg/view?usp=drive_web

Chromium browser for extention(Win_335181_chrome-win32.zip):
https://drive.google.com/file/d/0ByYhW8gwlrVuQm9mb2Jzc2MyWlU/view?usp=drive_web

Arduino ENC28J60 and Serial port with Chromium(Chrome) extention . 
Can work and as .html page too(but without serial port(usb)).
How to plug ENC68J60:
![Alt text](/img/HOW_TO_PLUG.jpg "Arduino with ENC28J60")

![Alt text](/img/arduino-ethernet-enc28j60-4.jpg "Arduino with ENC28J60.")

How to connect to board and throught serial port(usb):
![Alt text](/img/1.jpg "Arduino with serial port1")
![Alt text](/img/2.jpg "Arduino with serial port2")
![Alt text](/img/3.jpg "Arduino with serial port3")
![Alt text](/img/4.jpg "Arduino with serial port2")
![Alt text](/img/5.jpg "Arduino with serial port3")
![Alt text](/img/6.jpg "Arduino with serial port3")

Can send .doc file if you need (russian language :) )



