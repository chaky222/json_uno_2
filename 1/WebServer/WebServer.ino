
#include <SPI.h>
// #include <UIPEthernet.h>
#include <Ethernet.h>


// Enter a MAC address and IP address for your controller below.
// The IP address will be dependent on your local network:
byte mac[] = {0xDE, 0xAD, 0xBE, 0xEF, 0xFE, 0xED};
IPAddress ip(192, 168, 0, 177); 


// Initialize the Ethernet server library
// with the IP address and port you want to use
// (port 80 is default for HTTP):
EthernetServer server(80);

void setup() {
  // Open serial communications and wait for port to open:
  Serial.begin(9600);
  // while (!Serial) {
  //   ; // wait for serial port to connect. Needed for Leonardo only
  // }
  
  
  // start the Ethernet connection and the server:
  Ethernet.begin(mac, ip);
  server.begin();
  Serial.print("server is at ");
  Serial.println(Ethernet.localIP());
}

uint8_t income_size= 0;
static char *strchr_pointer; 
static char *get_str; 
char cmdbuffer[25];
bool get_finded =false;

float last_float = 0;
float last_float2 = 0;

// char cmdbuffer_serial[25];
// bool get_finded_serial =false;



void loop() {
  // listen for incoming clients
  EthernetClient client = server.available();
  if (client) {
    // Serial.println("new client");
    // an http request ends with a blank line
    boolean currentLineIsBlank = true;
    get_finded =false;
    while (client.connected()) {
      if (client.available()) {
        char c = client.read();
        // Serial.write(c);
        if ((!get_finded) && (income_size < 24)){
          cmdbuffer[income_size] = c;
          income_size++;
        }        
        // if you've gotten to the end of the line (received a newline
        // character) and the line is blank, the http request has ended,
        // so you can send a reply
        if (c == '\n' && currentLineIsBlank) {
          // send a standard http response header
          client.println("HTTP/1.1 200 OK");
          client.println("Content-type: application/javascript;");
          client.println("Access-Control-Allow-Origin: *");
          client.println("Connection: close;");  // the connection will be closed after completion of the response
          client.println();
          client.print("myJsonMethod({\"result\":1,\"value1\":");
          last_float = gen_random_float(last_float);
          client.print(last_float);
          client.print(",\"value2\":");
          last_float2 = gen_random_float(last_float2);
          client.print(last_float2);
          client.print("})");
          break;
        }
        if (c == '\n') {
          cmdbuffer[income_size] = '\0';
          get_str = strstr(cmdbuffer, "GET /?");
          if(get_str){
            get_finded = true;
            get_str=strstr(cmdbuffer, "?") + 1;
            char *p1 = strstr(get_str, " ");
            if(p1){
              p1[0] = '\0';
              // Serial.println(get_str);
              if (code_seen('G')){
                // Serial.print("G=");
                // Serial.println(code_value_long());
              }
              if (code_seen('M')){
                // Serial.print("M=");
                // Serial.println(code_value_long());
              }              
            }
          }          
          income_size = 0;
          currentLineIsBlank = true;
        }else if (c != '\r') {
          currentLineIsBlank = false;
        }
      }
    }
    // give the web browser time to receive the data
    delay(1);
    // close the connection:
    client.stop();
    // Serial.println("client disconnected");
  }
  if (Serial.available() > 0) {
    // get incoming byte:
    char c = Serial.read();
    // read first analog input, divide by 4 to make the range 0-255:
    if (c == 'A'){
      Serial.print("myJsonMethod({\"result\":1,\"value1\":");
      last_float = gen_random_float(last_float);
      Serial.print(last_float);
      Serial.print(",\"value2\":");
      last_float2 = gen_random_float(last_float2);
      Serial.print(last_float2);
      Serial.print("})");
    }
  }
}
float code_value(){ 
  return (strtod(&get_str[strchr_pointer - get_str + 1], NULL)); 
}

long code_value_long(){ 
  return (strtol(&get_str[strchr_pointer - get_str + 1], NULL, 10)); 
}

bool code_seen(char code){
  strchr_pointer = strchr(get_str, code);
  return (strchr_pointer != NULL);  //Return True if a character was found
}



float gen_random_float(float last_float){
  uint16_t last = trunc(last_float*100);
  uint16_t min = (last>1000) ? last-1000 : 0;
  uint16_t max = (last<9000) ? last+1000 : 10000;
  uint16_t randNumber = random(min, max);
  float result = randNumber/99.9;
  return result;
}

